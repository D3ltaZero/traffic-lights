package com.example.traffic_lights;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.view.View;
import android.os.Bundle;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {
   private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout = findViewById(R.id.linearLayout);
    }

    public void onClickRedButton(View view) {
        linearLayout.setBackgroundResource(R.color.red);
    }

    public void onClickYellowButton(View view) {
        linearLayout.setBackgroundResource(R.color.yellow);
    }

    public void onClickGreenButton(View view) {
        linearLayout.setBackgroundResource(R.color.green);
    }
}